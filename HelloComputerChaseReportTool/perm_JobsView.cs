//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace HelloComputerChaseReportTool
{
    using System;
    using System.Collections.Generic;
    
    public partial class perm_JobsView
    {
        public long RowNumber { get; set; }
        public Nullable<int> OpenDateYearFilter { get; set; }
        public string OpenDateMonthFilter { get; set; }
        public int JobID { get; set; }
        public string JobNo { get; set; }
        public string Client { get; set; }
        public string Campaign { get; set; }
        public string ProductName { get; set; }
        public Nullable<System.DateTime> OpenDate { get; set; }
        public Nullable<System.DateTime> CloseDate { get; set; }
        public string WorkType { get; set; }
        public double EstHours { get; set; }
        public double ActHours { get; set; }
        public double EstAmount { get; set; }
        public double ActAmount { get; set; }
        public double Variance { get; set; }
        public double VariancePerc { get; set; }
        public string Element { get; set; }
        public bool Cancelled { get; set; }
        public string Status { get; set; }
        public string UserName { get; set; }
    }
}
