﻿using System.Web;
using System.Web.Optimization;

namespace HelloComputerChaseReportTool
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Styles/css").Include(
                      "~/Styles/css/site.min.css"));

            // Pages 
            bundles.Add(new ScriptBundle("~/bundles/allPages").Include(
                      "~/Scripts/Pages/all.js"));

            // Data table CSS
            bundles.Add(new StyleBundle("~/Styles/dataTables").Include(
                      "~/Styles/_data_tables/shCore.css",
                      "~/Styles/_data_tables/dataTables.bootstrap.css"));

            bundles.Add(new ScriptBundle("~/bundles/HCDataSearch").Include(
                      "~/Scripts/underscore-min.js", 
                      "~/Scripts/Apps/SearchDataMesh.js"));

            // Data table scripts
            bundles.Add(new ScriptBundle("~/bundles/jQuerydataTables").Include(
                      "~/Scripts/_data_tables/jquery.dataTables.js"));

            bundles.Add(new ScriptBundle("~/bundles/dataTables").Include(
                      "~/Scripts/_data_tables/dataTables.bootstrap.js",
                      "~/Scripts/_data_tables/shCore.js"));
        }
    }
}
