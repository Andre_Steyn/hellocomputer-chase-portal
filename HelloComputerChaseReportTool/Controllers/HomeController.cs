﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Antlr.Runtime;
using AutoMapper;
using HelloComputerChaseReportTool.Models;
using Microsoft.Ajax.Utilities;

namespace HelloComputerChaseReportTool.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public JsonResult Search(string searchValue)
        {
            using (var db = new ChaseDataEntities())
            {
                var chaseData = db.perm_JobsView.Where(x=>(x.OpenDate.Value.Year.Equals(DateTime.Now.Year-1) || x.OpenDate.Value.Year.Equals(DateTime.Now.Year))).ToList();

                var searchResult = chaseData.Where(x=>
                (x.JobNo.ToLower().Contains(searchValue) || x.JobNo.ToLower().Equals(searchValue)) || 
                (x.Client.ToLower().Contains(searchValue) || x.Client.ToLower().Equals(searchValue)) || 
                (x.Element.ToLower().Contains(searchValue) || x.Element.ToLower().Equals(searchValue)) || 
                (x.UserName.ToLower().Contains(searchValue) || x.UserName.ToLower().Equals(searchValue))).DistinctBy(x=>x.JobNo).OrderBy(x=> x.Client).ThenBy(x=>x.Element).ToList();

                db.Dispose();

                return Json(searchResult, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult Report(int id)
        {
            using (var db = new ChaseDataEntities())
            {
                var chaseData = db.perm_JobsView.Where(x => (x.OpenDate.Value.Year.Equals(DateTime.Now.Year - 1) || x.OpenDate.Value.Year.Equals(DateTime.Now.Year))).ToList();
                var viewRecord = chaseData.Where(x => x.RowNumber.Equals(id)).ToList();

                var reportData = chaseData.Where(x => x.JobNo.Equals(viewRecord.FirstOrDefault().JobNo)).GroupBy(x=>x.WorkType.Trim()).ToList();

                double totalEstHours = 0;
                double totalEstAmount = 0;
                double totalActualHours = 0;
                double totalActualAmount = 0;
                double totalHourVariance = 0;
                double totalVarianceAmount = 0;
                double totalHoursVariancePerc = 0;
                double totalAmountVariance = 0;

                var groupedTotaled = new List<perm_JobsView>();

                foreach (var record in reportData)
                {
                    double actHrs = 0;
                    double actAmt = 0;
                    double vrnc = 0;
                    double vrncPerc = 0;

                    foreach (var item in record)
                    {
                        totalEstHours += item.EstHours;
                        totalEstAmount += item.EstAmount;
                        totalActualHours += item.ActHours;
                        totalActualAmount += item.ActAmount;
                        totalHourVariance += item.Variance;
                        totalVarianceAmount += 0;
                        totalHoursVariancePerc += item.VariancePerc;
                        totalAmountVariance += 0;

                        actHrs += item.ActHours;
                        actAmt += item.ActAmount;
                        vrnc += item.Variance;
                        vrncPerc += item.VariancePerc;
                    }

                    var newJob = new perm_JobsView()
                    {
                        OpenDateYearFilter = record.FirstOrDefault().OpenDateYearFilter,
                        OpenDateMonthFilter = record.FirstOrDefault().OpenDateMonthFilter,
                        JobNo = record.FirstOrDefault().JobNo,
                        Client = record.FirstOrDefault().Client,
                        Campaign = record.FirstOrDefault().Campaign,
                        ProductName = record.FirstOrDefault().ProductName,
                        OpenDate = record.FirstOrDefault().OpenDate,
                        CloseDate = record.FirstOrDefault().CloseDate,
                        WorkType = record.Key,
                        EstHours = record.FirstOrDefault().EstHours,
                        ActHours = actHrs,
                        EstAmount = record.FirstOrDefault().EstAmount,
                        ActAmount = actAmt,
                        Variance = vrnc,
                        VariancePerc = vrncPerc,
                        Element = record.FirstOrDefault().Element,
                        Cancelled = record.FirstOrDefault().Cancelled,
                        Status = record.FirstOrDefault().Status,
                        UserName = record.FirstOrDefault().UserName
                    };

                    groupedTotaled.Add(newJob);
                }

                var viewModel = new ChaseDataViewModel()
                {
                    TotalEstHours = totalEstHours,
                    TotalEstAmount = totalEstAmount,
                    TotalActualHours = totalActualHours,
                    TotalActualAmount = totalActualAmount,
                    TotalHourVariance = totalHourVariance,
                    TotalVarianceAmount = totalVarianceAmount,
                    TotalHoursVariancePerc = totalHoursVariancePerc,
                    TotalAmountVariance = totalAmountVariance,
                    JobData = groupedTotaled
                };

                return View(viewModel);
            }
            
        }

        [HttpPost]
        public ActionResult Results(string searchValue)
        {
            using (var db = new ChaseDataEntities())
            {
                var chaseData = db.perm_JobsView.Where(x => (x.OpenDate.Value.Year.Equals(DateTime.Now.Year - 1) || x.OpenDate.Value.Year.Equals(DateTime.Now.Year))).ToList();

                var searchResult = chaseData.Where(x =>
                (x.JobNo.ToLower().Contains(searchValue.ToLower()) || x.JobNo.ToLower().Equals(searchValue.ToLower())) ||
                (x.Client.ToLower().Contains(searchValue.ToLower()) || x.Client.ToLower().Equals(searchValue.ToLower())) ||
                (x.Element.ToLower().Contains(searchValue.ToLower()) || x.Element.ToLower().Equals(searchValue.ToLower())) ||
                (x.UserName.ToLower().Contains(searchValue.ToLower()) || x.UserName.ToLower().Equals(searchValue.ToLower()))).DistinctBy(x => x.JobNo).OrderBy(x => x.Client).ThenBy(x => x.Element).ToList();

                db.Dispose();

                //return Json(searchResult, JsonRequestBehavior.AllowGet);
                return View(searchResult);
            }

        }

    }
}