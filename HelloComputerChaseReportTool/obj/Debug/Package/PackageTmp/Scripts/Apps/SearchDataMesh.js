﻿$(function (window, document, $, _) {
    'use strict';

    // Functions
    //_____________________________________________________________________________________________
    function searchDataMesh(searchValue) {
        $.ajax({
            url: "/home/Search",
            data: { searchValue: searchValue.toLowerCase() },
            cache: false,
            success: function (jsonResult) {
                //console.log(jsonResult);

                var html = "";

                if (jsonResult.length === 0) {
                    html = "No Results Found...";
                } else {
                    html = "<table>";
                    _.each(jsonResult, function (item) {
                        html += "<tr><td><a href='/Home/Report?id=" + item.RowNumber + "'>" + item.JobNo + "</a></td><td><a href='/Home/Report?id=" + item.RowNumber + "'>" + item.Client + "</a></td><td><a href='/Home/Report?id=" + item.RowNumber + "'>" + item.Element + "</a></td></tr>";
                    });
                    html += "</table>";
                }
                $("#loader-gif").hide(function() {
                    $("#tablestuff").html(html);
                });
                
            }
        });
    }
    //_____________________________________________________________________________________________

    // Events
    //_____________________________________________________________________________________________
    //$("#search").on("click", function () {
    //    $("#tablestuff").html("");
    //    $("#loader-gif").show();
    //    var currentValue = $("#searchValue").val();

    //    if (currentValue.length >= 3) {
    //        searchDataMesh(currentValue);
    //    }
    //});
    //_____________________________________________________________________________________________

    // Init
    //_____________________________________________________________________________________________
    $("#loader-gif").hide();
    //_____________________________________________________________________________________________
}(window, document, jQuery, _));