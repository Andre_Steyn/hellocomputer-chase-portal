﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HelloComputerChaseReportTool.Models
{
    public class ChaseDataViewModel
    {
        public double TotalEstHours { get; set; }
        public double TotalEstAmount { get; set; }
        public double TotalActualHours { get; set; }
        public double TotalActualAmount { get; set; }
        public double TotalHourVariance { get; set; }
        public double TotalVarianceAmount { get; set; }
        public double TotalHoursVariancePerc { get; set; }
        public double TotalAmountVariance { get; set; }
        public List<perm_JobsView> JobData { get; set; } 
    }
}