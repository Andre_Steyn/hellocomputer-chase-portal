﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(HelloComputerChaseReportTool.Startup))]
namespace HelloComputerChaseReportTool
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
